'use strict';
 
angular.module('myApp').factory('UserService', ['$http', '$q', function($http, $q){
    
    var REST_SERVICE_URI = 'http://localhost:8082/rest/getCurrencyList';


 
    var factory = {
        sendMessage: sendMessage
    };
 
    return factory;
 
 
    function sendMessage(user) {
        var deferred = $q.defer();


         console.log("in sendMessage in service==>");
                var fd = new FormData();
               fd.append('email', user.email);
               fd.append('subject', user.subject);
               fd.append('message', user.message);
         $http({
            method: 'POST',
            url: REST_SERVICE_URI,
            headers: {
                'Content-Type': undefined,
                'boundary':'gc0p4Jq0M2Yt08jU534c0p'
                //'Content-Type':'application/x-www-form-urlencoded'
            },
            data: fd
        })
        .success(function (response) {
            console.log("response user service==>"+response);
        deferred.resolve(response);
        })
        .error(function (errResponse, status) {
        console.log("errResponse user service==>"+errResponse);
             console.log("status==>"+status);
                deferred.reject(errResponse);
        });

       
        return deferred.promise;
    }
 
 
  

 
}]);
