'use strict';
 
angular.module('myApp').controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
    var self = this;
    
    self.user={email:'',subject:'',message:''};
    self.users=[];
    self.statusMessage = "";
    self.emailStatus = null;
 
    self.submit = submit;
    self.reset = reset;
    /*$scope.uploadFile = function(){
               var file = $scope.myFile;
               
               console.log('file is ' );
               console.dir(file);
               UserService.uploadFile(file);
            };*/
 
   function setStatusMessage(string){
    self.statusMessage=string;
   }
 
    function sendMessage(user){
        console.log("inside sendmessage in user_controller==>");
        console.log("email==>"+user.email);
        console.log("subject==>"+user.subject);
        console.log("message==>"+user.message);
        UserService.sendMessage(user)
            .then(
            function(response) {
                self.emailStatus = true;
             setStatusMessage("Email Sucessfully Submitted");
             console.log("Email Sucessfully Submitted==>");
            },
            function(errResponse){
                self.emailStatus = false;
                setStatusMessage("Email Send Unsucessfull");
                console.log("Email Send Unsucessfull==>");
            }

        );
    }
 
 
    function submit() {
         console.log("in submit==>");
        sendMessage(self.user);
        reset();
    }
    function reset(){
         console.log("in reset==>");
        self.user={email:'',subject:'',message:''};
        
        $scope.myForm.$setPristine(); //reset Form
    }
 
}]);
